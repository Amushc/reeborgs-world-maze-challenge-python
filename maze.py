def turn_right():
    turn_left()
    turn_left()
    turn_left()
    move()

def check_wall():
    while not front_is_clear():
        turn_left()
        
while not at_goal():
    if right_is_clear():
        turn_right()
    elif not front_is_clear():
        check_wall()
        move()
    else:
        move()
        
